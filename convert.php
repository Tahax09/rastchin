<?php
require_once 'config.php';
require_once 'include/cssparser.php';
require_once 'include/rtl.php';
require_once 'include/convert.php';

if( !isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) || !( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) ){
	die("please don't use directly!"); // allow access....
}

if ( isset( $_REQUEST['ltr'] ) ) {
	if ( ! empty( $_REQUEST['ltr'] ) ) {
		convert( $_REQUEST['ltr'] );

	}
}

function convert( $raw_css ) {

	$complete = ( $_REQUEST['complete'] === 'true' ) ? true : false;
	$minify   = ( $_REQUEST['minify'] === 'true' ) ? true : false;
	$reset    = false;
	if ( ! $complete ) {
		$reset = ( $_REQUEST['reset'] === 'true' ) ? true : false;
	}


	$convert = new Convert( $raw_css );
	$rtl_css = $convert->get_rtl( $complete, $reset, $minify );

	$count_convert_file  = file_get_contents( 'files/count-convert.txt' );
	$count_convert_array = explode( ',', $count_convert_file );
	if ( count( $count_convert_array ) == 3 ) {
		$count_convert_array[0] ++;    // number of convert task.
		$count_convert_array[1] += ( $convert->property_count > 0 ) ? $convert->property_count : 0;// number of checked properties.
		$count_convert_array[2] += ( $convert->rtl_property_count > 0 ) ? $convert->rtl_property_count : 0;// number of converted properties.

		file_put_contents( 'files/count-convert.txt', implode( ',', $count_convert_array ) );
	}
	echo "/* " . $convert->rtl_property_count . " of " . $convert->property_count . " CSS properties converted by rtl.daskhat.ir \n These properties should be override originals. \n just load rtl css file after original css file.*/ \n\n/* body {direction:rtl;} */\n\n" . $rtl_css;

}









